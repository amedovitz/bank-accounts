package accounts;

public class LoanAccount extends Account{

    public LoanAccount (String customer, double balance, double interestRate){
        super(customer, balance, interestRate);
    }

    @Override
    public double calculateInterest(int numberOfMonths) {
        double interest = 0;
        if(this.getCustomer().equalsIgnoreCase("Individual")){
            return (numberOfMonths - 3) * this.getInterestRate();
        }else if(this.getCustomer().equalsIgnoreCase("Company")){
            return (numberOfMonths - 2) * this.getInterestRate();
        }
        return interest;
    }

   @Override
    public void depositingMoney(double amountOfMoney) {
        double newBalance = this.getBalance() + amountOfMoney;
        this.setBalance(newBalance);
    }
}
