package accounts;

public class DepositAccount extends Account{

    public DepositAccount (String customer, double balance, double interestRate){
        super(customer, balance, interestRate);
    }

    @Override
    public double calculateInterest(int numberOfMonths){
        if(this.getBalance()>0 && this.getBalance()<1000)
            return 0;
        else
            return numberOfMonths * this.getInterestRate();
    }

    @Override
    public void depositingMoney(double amountOfMoney) {
        double newBalance = this.getBalance() + amountOfMoney;
        this.setBalance(newBalance);
    }

    public void withdrawingMoney(double amountOfMoney){
        double newBalance = this.getBalance() - amountOfMoney;
        this.setBalance(newBalance);
     }


}
