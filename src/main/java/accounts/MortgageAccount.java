package accounts;

public class MortgageAccount extends Account{
    public MortgageAccount (String customer, double balance, double interestRate){
        super(customer, balance, interestRate);
    }

    @Override
    public double calculateInterest(int numberOfMonths) {
        double interest = 0;
        if(this.getCustomer().equalsIgnoreCase("Individual")){
            return (numberOfMonths - 6) * this.getInterestRate();
        }else if(this.getCustomer().equalsIgnoreCase("Company")){
            return (12 * this.getInterestRate())/2 + (numberOfMonths-12) * this.getInterestRate();
        }
        return interest;
    }

    @Override
    public void depositingMoney(double amountOfMoney) {
        double newBalance = this.getBalance() + amountOfMoney;
        this.setBalance(newBalance);
    }


}
