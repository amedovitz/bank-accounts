package accounts;

public abstract class Account {

    private String customer;
    private double balance;
    private double interestRate;

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public Account (String customer, double balance, double interestRate){
        this.customer = customer;
        this.balance = balance;
        this.interestRate = interestRate;
    }

    public abstract double calculateInterest(int numberOfMonths);

    public abstract void depositingMoney(double amountOfMoney);

}
