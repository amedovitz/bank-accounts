package clients;

import accounts.DepositAccount;
import accounts.LoanAccount;
import accounts.MortgageAccount;

public class Main {
    public static void main (String[] args){

        DepositAccount individualAccount = new DepositAccount("Individual", 250, 5);
        double interest = individualAccount.calculateInterest(24);
        System.out.println("Individual interest is: " + interest);
        individualAccount.withdrawingMoney(500);
        double newInterest = individualAccount.calculateInterest(24);
        System.out.println("Individual interest after withdrawing is: " + newInterest);

        LoanAccount individualLoanAccount = new LoanAccount("Individual", 0, 4.1);
        individualLoanAccount.depositingMoney(10000);
        double loanInterest = individualLoanAccount.calculateInterest(36);
        System.out.println("Individual loan interest is: " + loanInterest);

        LoanAccount companyLoanAccount = new LoanAccount("Company", 150000, 3.6);
        double companyLoanInterest = companyLoanAccount.calculateInterest(60);
        System.out.println("Company loan interest is: " + companyLoanInterest);

        MortgageAccount individualMortgageAccount = new MortgageAccount("Individual", 6000, 3.1);
        double individualMortgageInterest = individualMortgageAccount.calculateInterest(60);
        System.out.println("Individual mortgage interest is: " + individualMortgageInterest);

        MortgageAccount companyMortgageAccount = new MortgageAccount("Company", 150000, 2.9);
        double companyMortgageInterest = companyMortgageAccount.calculateInterest(120);
        System.out.println("Company mortgage interest is: " + companyMortgageInterest);


    }
}
